package integrados.sinais.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SinaisRest {

    @GetMapping("/login")
    public String fazLogin(){
        return "FEZ LOGIN";
    }
}
